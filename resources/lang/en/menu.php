<?php

return [

    'main_navigation'               => 'MAIN NAVIGATION',
    'blog'                          => 'Blog',
    'pages'                         => 'Pages',
//    'account_settings'              => 'ACCOUNT SETTINGS',
//    'profile'                       => 'Profile',

    'product'                       => 'Product',
    'list_product'                  => 'Products',
    'categories'                    => 'Categories',
    'brands'                        => 'Brands',
    'vendors'                       => 'Vendors',

//    'change_password'               => 'Change Password',
//    'multilevel'                    => 'Multi Level',
//    'level_one'                     => 'Level 1',
//    'level_two'                     => 'Level 2',
//    'level_three'                   => 'Level 3',
//    'labels'                        => 'LABELS',
//    'important'                     => 'Important',
//    'warning'                       => 'Warning',
//    'information'                   => 'Information',
];
