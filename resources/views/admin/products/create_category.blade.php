@extends('adminlte::page')

{{--@section('content_header')--}}
{{--    <strong>Thêm Sản phẩm1 111</strong>--}}
{{--@stop--}}
<link href="{{ asset('/css/category.css') }}" rel="stylesheet">

@section('content')
    <div class="card">
        <div class="box box-primary"  style="padding: 20px">
            <div class="box-header">
                <h3 style="text-align: center;">Add category</h3>
            </div><!-- /.box-header -->
            <!-- form start -->
            <form role="form" method="POST"   id="form" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="box-body">
                    @if(count($listCategory) > 0)
                    <div class="form-group col-md-12">
                        <label for="category_id">Category:</label>
                        <select name="category_id" id="category_id" class="form-control" >
                            <option value="0">Choose parent category</option>
                            @foreach($listCategory as $cate)
                                <option value="{{ $cate->id }}">{{ $cate->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    @endif

                    <div class="form-group col-md-12">
                        <label for="overview">Name: </label>
                        <input type="text" name="name" class="form-control" id="file-input" value="">
                    </div>

                    <div class="form-group">
                        <input type='file'  name="img_category" onchange="readURL(this);" required/>
                    </div>
                    <img id="blah" style="border: 1px solid red" src="{{URL::asset('./image/')}}/default.png" alt="your image" />

                </div><!-- /.box-body -->
                <div class="box-footer" style="margin-left: 40%">
                    <button type="submit" class="btn btn-primary">Add</button>
                    {{--                    <button type="reset" class="btn btn-default" style="margin-left: 3%">Làm mới</button>--}}
                </div>
            </form>
        </div><!-- /.box -->
    </div>
    </form>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script>
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#blah')
                        .attr('src', e.target.result);
                };
                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>
@stop
