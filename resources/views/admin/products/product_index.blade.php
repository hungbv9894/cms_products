@extends('adminlte::page')

@section('content')
    <div class="card">
        <div class="card-body">
        @include('admin.layouts.flash-msg')
            <!-- Search form -->
            <input style="float: left; width: 300px" class="form-control" type="text" placeholder="Search" aria-label="Search">
            <a href="{{ route('product.create') }}" type="button" class="btn btn-primary float-right">Add Product <i class="fa fa-plus"></i></a>
        </div>

        <div style="padding: 20px">
            <table class="table">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Category</th>
                    <th>Brand</th>
                    <th>Vendor</th>
                    <th>Image</th>
                    <th>Price</th>
                    <th>Sale price</th>
                    <th>Description</th>
                    <th></th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach($listProduct as $list)
                    <tr>
                        <td>{{$list->id}}</td>
                        <td>{{$list->name}}</td>
                        <td>{{$list->category->name}}</td>
                        <td>{{$list->brand->name}}</td>
                        <td>{{$list->vendor->name}}</td>
                        <td>
                            @if(count($list->fileImage) != 0)
                                <img class="img1" style=" height: 100px; width:150px; " src="{{ $list->fileImage[0]->path }}">
                            @endif
                        </td>
                        <td>{{$list->price}}</td>
                        <td>{{$list->sale_price}}</td>
                        <td>{{$list->description}}</td>
                        <td>
                            <a class="btn btn-white btn-bitbucket" href="{{ route('product.edit', $list->id) }}" ><i class="fas fa-edit"></i> Edit</a>
                            <a href="{{ route('product.delete', $list->id) }}" class="btn btn-white btn-bitbucket" data-method="DELETE"  onclick="return confirm('Are you sure to delete this product?')">
                                <i class="fa fa-trash"></i> Delete
                            </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@stop
