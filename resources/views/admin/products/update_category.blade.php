@extends('adminlte::page')

<link href="{{ asset('/css/category.css') }}" rel="stylesheet">

@section('content')
    <div class="card">
        <div class="box box-primary"  style="padding: 20px">
            <div class="box-header">
                <h3 style="text-align: center;">Edit category</h3>
            </div><!-- /.box-header -->
            <!-- form start -->
            <form role="form" method="POST"   id="form" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="box-body">
                    @if(count($listCategory) > 0)
                        <div class="form-group col-md-12">
                            <label for="category_id">Category:</label>
                            <select name="category_id" id="category_id" class="form-control" >
                                <option value="0">Choose parent category</option>
                                @foreach($listCategory as $cate)
                                    @if($cate->id != $category->id)
                                        <option value="{{ $cate->id }}" {{ $category->parent_id == $cate->id ? 'selected' : '' }}>{{ $cate->name }}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                    @endif

                    <div class="form-group col-md-12">
                        <label for="overview">Name: </label>
                        <input type="text" name="name" class="form-control" id="file-input" value="{{ $category->name }}">
                    </div>

                    <div class="form-group">
                        <input type='file'  name="img_category" onchange="readURL(this);" />
                    </div>
                    <img id="blah" style="border: 1px solid red" src="{{ $category->image }}" alt="your image" />

                </div><!-- /.box-body -->
                <div class="box-footer" style="margin-left: 40%">
                    <button type="submit" class="btn btn-primary">Edit</button>
                    {{--                    <button type="reset" class="btn btn-default" style="margin-left: 3%">Làm mới</button>--}}
                </div>
            </form>
        </div><!-- /.box -->
    </div>
    </form>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script>
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#blah')
                        .attr('src', e.target.result);
                };
                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>
@stop
