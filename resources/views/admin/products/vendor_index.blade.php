@extends('adminlte::page')

{{--@section('content_header')--}}
{{--    <strong>Category</strong>--}}
{{--@stop--}}

@section('content')
    <div class="card">

        <div class="card-body">
            @include('admin.layouts.flash-msg')
            <!-- Search form -->
            <input style="float: left; width: 300px" class="form-control" type="text" placeholder="Search" aria-label="Search">
            <button style="float: right" type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">Add Vendor <i class="fa fa-plus"></i></button>
        </div>

        <!-- Modal -->
        <div class="modal fade" id="myModal" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content" >
                    <div class="modal-header">
                        <h4 class="modal-title">Add new vendor</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                        <input class="form-control text-vendor" placeholder="Input vendor" name="name" type="text" id="name">
                    </div>
                    <div class="modal-footer">
                        <button onclick="newVendor()" class="btn btn-primary" >Add</button>
                        <input name="_token" id="_token" type="hidden" value="{{ csrf_token() }}">
                    </div>
                    <!-- </form> -->
                </div>
            </div>
        </div>

        <!-- Modal -->
        <div class="modal fade" id="editVendor" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content" >
                    <div class="modal-header">
                        <h4 class="modal-title">Edit vendor</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                        <input class="form-control text-vendor" name="name" type="text" id="name_edit" value="">
                        <input hidden name="id_vendor" type="text" id="id_vendor" value="">
                    </div>
                    <div class="modal-footer">
                        <button onclick="editCategory()" class="btn btn-primary" >Edit</button>
                        <input name="_token" id="_token" type="hidden" value="{{ csrf_token() }}">
                    </div>
                    <!-- </form> -->
                </div>
            </div>
        </div>

        <div style="padding: 20px">
            <table class="table">
                <thead>
                <tr>
                    <th>Vendor ID</th>
                    <th>Name</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($listCategory as $list)
                    <tr>
                        <td>{{$list->id}}</td>
                        <td>{{$list->name}}</td>
                        <td>
                            <a class="btn btn-white btn-bitbucket edit-vendor" data-toggle="modal" data-target="#editVendor" data-id-vendor="{{ $list->id }}" data-name="{{ $list->name }}"><i class="fas fa-edit"></i> Edit</a>
                            <a href="{{ route('admin_vendor_delete', $list->id) }}" class="btn btn-white btn-bitbucket" data-method="DELETE" onclick="return confirm('Are you sure to delete this vendor?')">
                                <i class="fa fa-trash"></i> Delete
                            </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

    <script src="//ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script >
    function newVendor() {
        var name = $('.text-vendor').val();
        var token = $('#_token').val()
        $.ajax({
            type : 'POST',
            url : '{{ route('admin_vendor_create') }}',
            data :
                {
                    'name_vendor' : name,
                    '_token' : token,
                },
            success: function(data)
            {
                location.reload();
            },
            error: function(data)
            {
                var error = jQuery.parseJSON(data.responseText);
                $('.modal-body').append('<div class="alert alert-danger" style="margin-top:10px; margin-bottom:none;"><ul><li>' + error.message + '</li></ul></div>');
            }
        });
    }
    $('.edit-vendor').on('click', function () {
        $('#name_edit').val($(this).attr('data-name'));
        $('#id_vendor').val($(this).attr('data-id-vendor'));
    });
    function editCategory() {
        var name = $('#name_edit').val();
        var id = $('#id_vendor').val();
        var token = $('#_token').val();
        $.ajax({
            type : 'POST',
            url : '{{ route('admin_vendor_edit') }}',
            data :
                {
                    'name' : name,
                    'id': id,
                    '_token' : token,
                },
            success: function()
            {
                location.reload();
            },
            error: function(data)
            {
                var error = jQuery.parseJSON(data.responseText);
                $('.modal-body').append('<div class="alert alert-danger" style="margin-top:10px; margin-bottom:none;"><ul><li>' + error.message + '</li></ul></div>');
            }
        });
    }
</script>
@stop
