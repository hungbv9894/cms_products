@extends('adminlte::page')

{{--@section('content_header')--}}
{{--    <strong>Category</strong>--}}
{{--@stop--}}

@section('content')
    <div class="card">
        <div class="card-body">
        @include('admin.layouts.flash-msg')
            <!-- Search form -->
            <input style="float: left; width: 300px" class="form-control" type="text" placeholder="Search" aria-label="Search">
            <a style="float: right" href="{{ route('category.create') }}" type="button" class="btn btn-primary">Add Category<i class="fa fa-plus"></i></a>
        </div>

        <!-- Modal -->
        <div class="modal fade" id="myModal" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content" >
                    <div class="modal-header">
                        <h4 class="modal-title">Add new category</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                        <input class="form-control text-category" placeholder="input category" name="name" type="text" id="name">
                    </div>
                    <div class="modal-footer">
                        <button onclick="newcategories()" class="btn btn-primary" >Add</button>
                        <input name="_token" id="_token" type="hidden" value="{{ csrf_token() }}">
                    </div>
                    <!-- </form> -->
                </div>
            </div>
        </div>

        <div style="padding: 20px">
            <table class="table">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Parent ID</th>
                    <th>Name</th>
                    <th>Image</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($listCategory as $list)
                <tr>
                    <td>{{$list->id}}</td>
                    <td>{{$list->parentCate ?  $list->parentCate->name : ''}}</td>
                    <td>{{$list->name}}</td>
                    <td>
                        <img class="img1" style=" height: 100px; width:150px; " src="{{$list->image}}">
                    </td>
                    <td>
                        <a class="btn btn-white btn-bitbucket" href="{{ route('category.edit', $list->id) }}" ><i class="fas fa-edit"></i> Edit</a>
                        <a href="{{ route('category.delete', $list->id) }}" class="btn btn-white btn-bitbucket" data-method="DELETE" onclick="return confirm('Are you sure to delete this category? This action will delete category children, product and image relationship!')">
                            <i class="fa fa-trash"></i> Delete
                        </a>
                    </td>
                </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@stop
