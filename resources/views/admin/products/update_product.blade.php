@extends('adminlte::page')

<style>
    .image {
        position: relative;
        text-align: center;
        color: red;
    }

    .top-right {
        position: absolute;
        top: -8px;
        right: 15px;
        background-color: pink;
        border-radius: 15px;
        width: 25px;
        cursor: pointer;
    }
</style>

@section('content')
    <div class="card">
        <div class="box box-primary"  style="padding: 20px">
            <div class="box-header">
                <h3 style="text-align: center;">Edit product</h3>
            </div><!-- /.box-header -->
            <!-- form start -->
            <form role="form" method="POST" enctype="multipart/form-data" id="form" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="box-body">
                    <div class="form-group col-md-12">
                        <label for="category_id">Category:</label>
                        <select name="category_id" id="category_id" class="form-control" >
                            @foreach($listCategory as $cate)
                                <option value="{{ $cate->id }}" {{ $product->category_id == $cate->id ? 'selected' : 'id' }}>{{ $cate->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-md-12">
                        <label for="brand_id">Brand: </label>
                        <select name="brand_id" id="brand_id" class="form-control" >
                            @foreach($listBrand as $brand)
                                <option value="{{ $brand->id }}" {{ $product->brand_id == $brand->id ? 'selected' : 'id' }}>{{ $brand->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-md-12">
                        <label for="vendor_id">Vendor: </label>
                        <select name="vendor_id" id="vendor_id" class="form-control">
                            @foreach($listVendor as $vendor)
                                <option value="{{ $vendor->id }}" {{ $product->vendor_id == $vendor->id ? 'selected' : 'id' }}>{{ $vendor->name }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group col-md-12">
                        <label for="overview">Name: </label>
                        <input type="text" name="name" class="form-control" id="name" value="{{ $product->name }}">
                    </div>

                    <div class="form-group col-md-12">
                        <label for="overview">Image: </label>
                        <div id="filediv"><input name="file[]" type="file" id="file"/></div>
                        <input type="button" id="add_more" class="upload" value="Add More Files"/>
                        <br>
                        <label for="overview">Images uploaded: </label>
                        <div style="margin-top: 10px">
                            @foreach($product->fileImage as $image)
                                <div class="col-md-2 float-left image">
                                    <img src="{{ $image->path }}" alt="" width="120" height="120" class="img">
                                    <div class="top-right">x</div>
                                </div>
                            @endforeach
                        </div>
                        <input id="image_delete" value="" hidden name="image_delete"/>
                    </div>
                    <div style="clear: both"></div>
                    <div class="form-group col-md-12">
                        <label for="price">Price: </label>
                        <input type="text" name="price" class="form-control" id="price" value="{{ $product->price }}">
                    </div>
                    <div class="form-group col-md-12">
                        <label for="sale_price">Sale price: </label>
                        <input type="text" name="sale_price" class="form-control" id="sale_price" value="{{ $product->sale_price }}">
                    </div>
                    <div class="form-group col-md-12">
                        <label for="description" style="margin-top: 15px;">Description: </label>
                        <br>
                        <textarea name="description" id="description" rows="7" style="width: 100%;">{{ $product->description }}</textarea>
                    </div>
                </div><!-- /.box-body -->
                <div class="box-footer" style="margin-left: 40%">
                    <button type="submit" class="btn btn-primary">Edit</button>
                    {{--                    <button type="reset" class="btn btn-default" style="margin-left: 3%">Làm mới</button>--}}
                </div>
            </form>
        </div><!-- /.box -->
    </div>
    </form>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script type="text/javascript">
        var abc = 0;
        $(document).ready(function() {
            var imageDelete = $('#image_delete').val();
            $('.top-right').click(function () {
                imageDelete += $(this).closest('.image').find('img').attr('src') + ';';
                $('#image_delete').val(imageDelete);
                 $(this).closest('.image').hide();
            });
            function readURL(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function(e) {
                        $('#blah').attr('src', e.target.result);
                    }
                    reader.readAsDataURL(input.files[0]);
                }
            }

            $("#imgInp").change(function() {
                readURL(this);
            });

            $(".btn-success").click(function(){
                var html = $(".clone").html();
                $(".increment").after(html);
            });

            $("body").on("click",".btn-danger",function(){
                $(this).parents(".control-group").remove();
            });

            //  To add new input file field dynamically, on click of "Add More Files" button below function will be executed.
            $('#add_more').click(function() {
                $(this).before($("<div/>", {
                    id: 'filediv'
                }).fadeIn('slow').append($("<input/>", {
                    name: 'file[]',
                    type: 'file',
                    id: 'file'
                }), $("<br/><br/>")));
            });
            // Following function will executes on change event of file input to select different file.
            $('body').on('change', '#file', function() {
                if (this.files && this.files[0]) {
                    abc += 1; // Incrementing global variable by 1.
                    var z = abc - 1;
                    var x = $(this).parent().find('#previewimg' + z).remove();
                    $(this).before("<div id='abcd" + abc + "' class='abcd'><img style='width: 150px' id='previewimg" + abc + "' src=''/></div>");
                    var reader = new FileReader();
                    reader.onload = imageIsLoaded;
                    reader.readAsDataURL(this.files[0]);
                    $(this).hide();
                    $("#abcd" + abc).append($("<img/>", {
                        id: 'img',
                        src: "{{URL::asset('./image/')}}/close-img.png",
                        alt: 'delete',
                        width: '10px',
                    }).click(function() {
                        $(this).parent().parent().remove();
                    }));
                }
            });
// To Preview Image
            function imageIsLoaded(e) {
                $('#previewimg' + abc).attr('src', e.target.result);
            };
            $('#upload').click(function(e) {
                var name = $(":file").val();
                if (!name) {
                    alert("First Image Must Be Selected");
                    e.preventDefault();
                }
            });
        });
    </script>
@stop
