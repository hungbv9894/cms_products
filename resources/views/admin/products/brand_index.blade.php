@extends('adminlte::page')

{{--@section('content_header')--}}
{{--    <strong>Category</strong>--}}
{{--@stop--}}

@section('content')
    <div class="card">
        <div class="card-body">
            @include('admin.layouts.flash-msg')
            <!-- Search form -->
            <input style="float: left; width: 300px" class="form-control" type="text" placeholder="Search" aria-label="Search">
            <button style="float: right" type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">Add Brand <i class="fa fa-plus"></i></button>
        </div>

        <!-- Modal -->
        <div class="modal fade" id="myModal" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content" >
                    <div class="modal-header">
                        <h4 class="modal-title">Add new brand</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                        <input class="form-control text-brand" placeholder="Input brand" name="name" type="text" id="name">
                    </div>
                    <div class="modal-footer">
                        <button onclick="newBrand()" class="btn btn-primary" >Add</button>
                        <input name="_token" id="_token" type="hidden" value="{{ csrf_token() }}">
                    </div>
                    <!-- </form> -->
                </div>
            </div>
        </div>

        <div class="modal fade" id="editBrand" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content" >
                    <div class="modal-header">
                        <h4 class="modal-title">Edit brand</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                        <input class="form-control text-brand" name="name" type="text" id="name-edit">
                        <input hidden name="id_brand" type="text" id="id_brand">
                    </div>
                    <div class="modal-footer">
                        <button onclick="editBrand()" class="btn btn-primary" >Edit</button>
                        <input name="_token" id="_token" type="hidden" value="{{ csrf_token() }}">
                    </div>
                    <!-- </form> -->
                </div>
            </div>
        </div>

        <div style="padding: 20px">
            <table class="table">
                <thead>
                    <tr>
                        <th>Brand ID</th>
                        <th>Name</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($listCategory as $list)
                    <tr>
                        <td>{{$list->id}}</td>
                        <td>{{$list->name}}</td>
                        <td>
                            <a class="btn btn-white btn-bitbucket edit-brand" data-toggle="modal" data-target="#editBrand" data-id-brand="{{ $list->id }}" data-name="{{ $list->name }}"><i class="fas fa-edit"></i> Edit</a>
                            <a href="{{ route('admin_brand_delete', $list->id) }}" class="btn btn-white btn-bitbucket" data-method="DELETE" onclick="return confirm('Are you sure to delete this vendor?')">
                                <i class="fa fa-trash"></i> Delete
                            </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script >
    function newBrand() {
        var name = $('.text-brand').val();
        var token = $('#_token').val()
        $.ajax({
            type : 'POST',
            url : '{{ route('admin_brand_create') }}',
            data :
                {
                    'name_brand' : name,
                    '_token' : token,
                },
            success: function(data)
            {
                location.reload();
            },
            error: function(data)
            {
                var error = jQuery.parseJSON(data.responseText);
                $('.modal-body').append('<div class="alert alert-danger" style="margin-top:10px; margin-bottom:none;"><ul><li>' + error.message + '</li></ul></div>');
            }
        });
    }
    $('.edit-brand').click(function () {
        $('#name-edit').val($(this).attr('data-name'));
        $('#id_brand').val($(this).attr('data-id-brand'));
    });
    function editBrand() {
        var name = $('#name-edit').val();
        var id = $('#id_brand').val();
        var token = $('#_token').val()
        $.ajax({
            type : 'POST',
            url : '{{ route('admin_brand_edit') }}',
            data :
                {
                    'name' : name,
                    'id' : id,
                    '_token' : token,
                },
            success: function(data)
            {
                location.reload();
            },
            error: function(data)
            {
                var error = jQuery.parseJSON(data.responseText);
                $('.modal-body').append('<div class="alert alert-danger" style="margin-top:10px; margin-bottom:none;"><ul><li>' + error.message + '</li></ul></div>');
            }
        });
    }
</script>
@stop
