@extends('adminlte::page')

@section('content')
    <div class="card">
        <div class="box box-primary"  style="padding: 20px">
            <div class="box-header">
                <h3 style="text-align: center;">Add product</h3>
            </div><!-- /.box-header -->
            <!-- form start -->
            <form role="form" method="POST" enctype="multipart/form-data" id="form" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="box-body">
                    <div class="form-group col-md-12">
                        <label for="category_id">Category:</label>
                        <select name="category_id" id="category_id" class="form-control" >
                            @foreach($listCategory as $cate)
                                <option value="{{ $cate->id }}">{{ $cate->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-md-12">
                        <label for="brand_id">Brand: </label>
                        <select name="brand_id" id="brand_id" class="form-control" >
                            @foreach($listBrand as $brand)
                                <option value="{{ $brand->id }}">{{ $brand->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-md-12">
                        <label for="vendor_id">Vendor: </label>
                        <select name="vendor_id" id="vendor_id" class="form-control">
                            @foreach($listVendor as $vendor)
                                <option value="{{ $vendor->id }}">{{ $vendor->name }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group col-md-12">
                        <label for="overview">Name: </label>
                        <input type="text" name="name" class="form-control" id="name" value="" required>
                    </div>

                    <div class="form-group col-md-12">
                        <div id="filediv">
                            <input name="file[]" type="file" id="file"/>
                        </div>
                        <input type="button" id="add_more" class="upload" value="Add More Files"/>
                    </div>

                    <div class="form-group col-md-12">
                        <label for="price">Price: </label>
                        <input type="text" name="price" class="form-control" id="price" value="">
                    </div>
                    <div class="form-group col-md-12">
                        <label for="sale_price">Sale price: </label>
                        <input type="text" name="sale_price" class="form-control" id="sale_price" value="">
                    </div>
                    <div class="form-group col-md-12">
                        <label for="description" style="margin-top: 15px;">Description: </label>
                        <br>
                        <textarea name="description" id="description" rows="7" style="width: 100%;" required></textarea>
                    </div>
                </div><!-- /.box-body -->
                <div class="box-footer" style="margin-left: 40%">
                    <button type="submit" class="btn btn-primary">Add</button>
{{--                    <button type="reset" class="btn btn-default" style="margin-left: 3%">Làm mới</button>--}}
                </div>
            </form>
        </div><!-- /.box -->
    </div>
    </form>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script type="text/javascript">
        var abc = 0;
        $(document).ready(function() {
            function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    $('#blah').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
                }
            }

            $("#imgInp").change(function() {
                readURL(this);
            });

            $(".btn-success").click(function(){
                var html = $(".clone").html();
                $(".increment").after(html);
            });

            $("body").on("click",".btn-danger",function(){
                $(this).parents(".control-group").remove();
            });

            //  To add new input file field dynamically, on click of "Add More Files" button below function will be executed.
            $('#add_more').click(function() {
                $(this).before($("<div/>", {
                    id: 'filediv'
                }).fadeIn('slow').append($("<input/>", {
                    name: 'file[]',
                    type: 'file',
                    id: 'file'
                }), $("<br/><br/>")));
            });
            // Following function will executes on change event of file input to select different file.
            $('body').on('change', '#file', function() {
                if (this.files && this.files[0]) {
                    abc += 1; // Incrementing global variable by 1.
                    var z = abc - 1;
                    var x = $(this).parent().find('#previewimg' + z).remove();
                    $(this).before("<div id='abcd" + abc + "' class='abcd'><img style='width: 150px' id='previewimg" + abc + "' src=''/></div>");
                    var reader = new FileReader();
                    reader.onload = imageIsLoaded;
                    reader.readAsDataURL(this.files[0]);
                    $(this).hide();
                    $("#abcd" + abc).append($("<img/>", {
                        id: 'img',
                        src: "{{URL::asset('./image/')}}/close-img.png",
                        alt: 'delete',
                        width: '10px',
                    }).click(function() {
                        $(this).parent().parent().remove();
                    }));
                }
            });
// To Preview Image
            function imageIsLoaded(e) {
                $('#previewimg' + abc).attr('src', e.target.result);
            };
            $('#upload').click(function(e) {
                var name = $(":file").val();
                if (!name) {
                    alert("First Image Must Be Selected");
                    e.preventDefault();
                }
            });
        });
    </script>
@stop
