<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Model\FileImage;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use SoftDeletes;

    protected $table = 'products';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'user_id', 'category_id', 'brand_id', 'vendor_id', 'price', 'sale_price', 'description', 'status', 'hot_flag',
    ];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public function category()
    {
        return $this->belongsTo('App\Model\Category', 'category_id', 'id');
    }

    public function brand()
    {
        return $this->belongsTo('App\Model\Brand', 'brand_id', 'id');
    }

    public function vendor()
    {
        return $this->belongsTo('App\Model\Vendor', 'vendor_id', 'id');
    }

    public function fileImage()
    {
        return $this->hasMany('App\Model\FileImage', 'product_id');
    }

    public function getImage($id)
    {
        $path_img = null;
        $picture = FileImage::where('product_id', $id)->first();
        if ($picture) {
            $path_img = $picture->path;
        }
        return $path_img;
    }
}
