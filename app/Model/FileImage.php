<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FileImage extends Model
{
    use SoftDeletes;
    
    protected $table = 'file_images';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'product_id','category_id', 'path'
    ];

    public function product()
    {
        return $this->belongsTo('App\Model\Product', 'product_id', 'id');
    }

    public function category()
    {
        return $this->belongsTo('App\Model\Category', 'category_id', 'id');
    }
}
