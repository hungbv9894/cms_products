<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AttributeValue extends Model
{
    use SoftDeletes;
    
    protected $table = 'attribute_value';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'attribute_id', 'value'
    ];

    public function attributes()
    {
        return $this->belongsTo('App\Model\Attribute', 'attribute_id', 'id');
    }
}
