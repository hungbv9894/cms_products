<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Model\FileImage;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    use SoftDeletes;

    protected $table = 'categories';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'user_id', 'parent_id'
    ];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public function parentCate()
    {
        return $this->belongsTo('App\Model\Category', 'parent_id', 'id');
    }

    public function children()
    {
        return $this->hasMany('App\Model\Category', 'parent_id', 'id');
    }
}
