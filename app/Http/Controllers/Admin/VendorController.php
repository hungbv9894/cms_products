<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Model\Vendor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class VendorController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $listCategory = Vendor::all();
        return view('admin.products.vendor_index', compact("listCategory")) ;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function create(Request $request)
    {
        $vendor = new Vendor();
        $vendor->name = $request['name_vendor'];
        $vendor->user_id = Auth::id();
        if($vendor->save()){
            return response()->json([
                'success' => true,
                'message' => 'Thành công'
            ], 200);
        }
        return response()->json([
            'success' => false,
            'message' => 'Upload fail'
        ], 422);
    }

    public function update(Request $request)
    {
        $vendor = Vendor::find($request['id']);
        $vendor->name = $request['name'];

        if($vendor->save()){
            return response()->json([
                'success' => true,
                'message' => 'Thành công'
            ], 200);
        }
        return response()->json([
            'success' => false,
            'message' => 'Update fail'
        ], 422);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function delete(Request $request)
    {
        if(Vendor::destroy($request['id'])) {
            return redirect()->back()->with('success', 'Delete successful!');
        }
        return redirect()->back()->with('fail', 'Delete fail!');
    }
}
