<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Model\Brand;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class BrandController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $listCategory = Brand::all();
        return view('admin.products.brand_index', compact("listCategory")) ;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function create(Request $request)
    {
        $brand = new Brand();
        $brand->name = $request['name_brand'];
        $brand->user_id = Auth::id();
        if($brand->save()){
            return response()->json([
                'success' => true,
                'message' => 'Thành công'
            ], 200);
        }
        $errors = $validation->errors();
        $errors =  json_decode($errors);
        return response()->json([
            'success' => false,
            'message' => $errors
        ], 422);
    }

    public function update(Request $request)
    {
        $brand = Brand::find($request['id']);
        $brand->name = $request['name'];

        if($brand->save()){
            return response()->json([
                'success' => true,
                'message' => 'Thành công'
            ], 200);
        }
        return response()->json([
            'success' => false,
            'message' => 'Update fail'
        ], 422);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function delete(Request $request)
    {
        if(Brand::destroy($request['id'])) {
            return redirect()->back()->with('success', 'Delete successful!');
        }
        return redirect()->back()->with('fail', 'Delete fail!');
    }
}
