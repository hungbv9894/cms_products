<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Model\Brand;
use App\Model\Category;
use App\Model\Product;
use App\Model\Vendor;
use Illuminate\Http\Request;
use App\Model\FileImage;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ProductController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $listProduct = Product::all();
        return view('admin.products.product_index', compact("listProduct")) ;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function create()
    {
        $listCategory = Category::all();
        $listBrand = Brand::all();
        $listVendor = Vendor::all();

        return view(
            'admin.products.create_product',
            ['listCategory' => $listCategory, 'listBrand' => $listBrand, 'listVendor' => $listVendor]
        );
    }

    public function store(Request $request)
    {
        DB::transaction(function () use ($request) {
            $product = new Product();
            $product->user_id = Auth::id();
            $product->category_id = $request->category_id;
            $product->brand_id = $request->brand_id;
            $product->vendor_id = $request->vendor_id;
            $product->name = $request->name;
            $product->price = $request->price;
            $product->sale_price = $request->sale_price;
            $product->description = $request->description;
            $product->status = 1;
            $product->hot_flag = 1;
            $product->save();
            if ($request->hasfile('file')) {
                $listImgs = $request->file('file');
                $i = 1;
                foreach ($listImgs as $photo) {
                    $uploadname = 'product_' . $product->id . date("yyyy-mm-dd") . '_' . $i . '.' . $photo->getClientOriginalExtension();
                    $photo->move(base_path() . '/public/uploads/img-products/', $uploadname);
                    $file_image = new FileImage();
                    $file_image->product_id = $product->id;
                    $file_image->name = $uploadname;
                    $file_image->path = '/uploads/img-products/' . $uploadname;
                    $file_image->save();
                    $i++;
                }
            }
        });

        return redirect()->route('product');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function edit($id)
    {
        $listCategory = Category::all();
        $listBrand = Brand::all();
        $listVendor = Vendor::all();
        $product = Product::find($id);

        return view(
            'admin.products.update_product',
            ['listCategory' => $listCategory, 'listBrand' => $listBrand, 'listVendor' => $listVendor, 'product' => $product]
        );
    }

    public function update(Request $request, $id)
    {
        DB::transaction(function () use ($request, $id) {
            $product = Product::find($id);
            $product->category_id = $request->category_id;
            $product->brand_id = $request->brand_id;
            $product->vendor_id = $request->vendor_id;
            $product->name = $request->name;
            $product->price = $request->price;
            $product->sale_price = $request->sale_price;
            $product->description = $request->description;
            $product->status = 1;
            $product->hot_flag = 1;
            $product->save();

            $imagesDelete = explode(';', $request->image_delete);
            foreach ($imagesDelete as $image) {
                if ($image != "") {
                    $fileImageDelete = FileImage::where('path', $image)->first();
                    if ($fileImageDelete) {
                        $fileImageDelete->delete();
                    }
                }
            }
            if( $request->hasfile('file')){
                $listImgs = $request->file('file');
                $numberOfImage = count(FileImage::all());
                $i = $numberOfImage + 1;
                foreach ($listImgs as $photo) {
                    $uploadname = 'product_' . $product->id . date("yyyy-mm-dd") . '_' . $i .  '.' . $photo->getClientOriginalExtension();
                    $photo->move(base_path() . '/public/uploads/img-products/', $uploadname);
                    $file_image = new FileImage();
                    $file_image->product_id = $product->id;
                    $file_image->name = $uploadname;
                    $file_image->path = '/uploads/img-products/' . $uploadname;
                    $file_image->save();
                    $i++;
                }
            }
        });


        return redirect()->route('product');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function delete($id)
    {
        DB::transaction(function () use ($id) {
            $fileImages = FileImage::where('product_id', $id)->get();
            foreach ($fileImages as $image) {
                $image->delete();
            }

            Product::destroy($id);
        });
        return redirect()->route('product')->with('success', 'Delete successful');
    }
}
