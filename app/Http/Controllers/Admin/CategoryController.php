<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Model\Category;
use App\Model\FileImage;
use App\Model\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class CategoryController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $listCategory = Category::all();
        return view('admin.products.category_index', compact("listCategory")) ;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function create()
    {
        $listCategory = Category::all();
        return view('admin.products.create_category', compact("listCategory")) ;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function store(Request $request)
    {
        $category = new Category();
        $category->user_id = Auth::id();
        if($request->category_id && $request->category_id != 0) {
            $category->parent_id = $request->category_id;
        }
        $category->name = $request->name;
        if( $request->hasfile('img_category')){
            $img_category = $request->file('img_category');
            $uploadname = 'category_' . date('Ymd') . '_' . time() . '.' . $img_category->getClientOriginalExtension();
            $img_category->move(base_path() . '/public/uploads/img-categories', $uploadname);
            $category->image = '/uploads/img-categories/' . $uploadname;
        }
        $category->save();
        return redirect()->route('category');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function edit($id)
    {
        $listCategory = Category::all();
        $category = Category::find($id);
        return view('admin.products.update_category', ['listCategory' => $listCategory, 'category' => $category]) ;
    }

    public function update(Request $request, $id)
    {
        $category = Category::find($id);
        if($request->category_id && $request->category_id != 0) {
            $category->parent_id = $request->category_id;
        }
        $category->name = $request->name;
        if( $request->hasfile('img_category')){
            $img_category = $request->file('img_category');
            $uploadname = 'category_' . date('Ymd') . '_' . time() . '.' . $img_category->getClientOriginalExtension();
            $img_category->move(base_path() . '/public/uploads/img-categories', $uploadname);
            $category->image = '/uploads/img-categories/' . $uploadname;
        }
        $category->save();
        return redirect()->route('category');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function delete($id)
    {
        DB::transaction(function () use ($id) {
            $category = Category::findOrFail($id);
            $categoryIds = $this->getChildren($category);
            array_push($categoryIds, (int)$id);
            $productIds = [];
            foreach ($categoryIds as $cateId) {
                $productIds = array_merge($productIds, Product::select('id')->where('category_id', $cateId)->get()->toArray());
            }
            $fileImageIds = [];
            foreach ($productIds as $product) {
                $fileImageIds = array_merge($fileImageIds, FileImage::select('id')->where('product_id', $product['id'])->get()->toArray());
            }
            foreach ($fileImageIds as $fileImage) {
                FileImage::destroy($fileImage['id']);
            }

            foreach ($productIds as $product) {
                Product::destroy($product['id']);
            }

            foreach ($categoryIds as $cate) {
                Category::destroy($cate);
            }
        });

        return redirect()->route('category')->with('success', 'Delete category successful!');
    }

    public function getChildren($category)
    {
        $ids = [];
        foreach ($category->children as $cate) {
            $ids[] = $cate->id;
            $ids = array_merge($ids, $this->getChildren($cate));
        }

        return $ids;
    }
}
