<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');



Route::group(['prefix' => 'admin', 'middleware' => 'auth'], function(){
    Route::group(['prefix' => 'product'], function () {
        Route::get('list', 'Admin\ProductController@index')->name('product');
        Route::get('create', 'Admin\ProductController@create')->name('product.create');
        Route::post('create', 'Admin\ProductController@store')->name('product.store');
        Route::get('edit/{id}', 'Admin\ProductController@edit')->name('product.edit');
        Route::post('edit/{id}', 'Admin\ProductController@update')->name('product.update');
        Route::get('delete/{id}', 'Admin\ProductController@delete')->name('product.delete');
    });

    Route::group(['prefix' => 'category'], function () {
        Route::get('list', 'Admin\CategoryController@index')->name('category');
        Route::get('create', 'Admin\CategoryController@create')->name('category.create');
        Route::post('create', 'Admin\CategoryController@store')->name('category.store');
        Route::get('edit/{id}', 'Admin\CategoryController@edit')->name('category.edit');
        Route::post('edit/{id}', 'Admin\CategoryController@update')->name('category.update');
        Route::get('delete/{id}', 'Admin\CategoryController@delete')->name('category.delete');
    });

    Route::group(['prefix' => 'brand'], function () {
        Route::get('list', 'Admin\BrandController@index')->name('brand');
        Route::post('create', 'Admin\BrandController@create')->name('admin_brand_create');
        Route::post('update', 'Admin\BrandController@update')->name('admin_brand_edit');
        Route::get('delete/{id}', 'Admin\BrandController@delete')->name('admin_brand_delete');
    });

    Route::group(['prefix' => 'vendor'], function () {
        Route::get('list', 'Admin\VendorController@index')->name('vendor');
        Route::post('create', 'Admin\VendorController@create')->name('admin_vendor_create');
        Route::post('update', 'Admin\VendorController@update')->name('admin_vendor_edit');
        Route::get('delete/{id}', 'Admin\VendorController@delete')->name('admin_vendor_delete');
    });
});
